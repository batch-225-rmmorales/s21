//

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

let studentNumbers = ['2020-1923', '2020-1924','2020-1925', '2020-1926', '2020-1927'];

console.log(studentNumbers);

// Common example of arrays

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

let mixedArray = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArray);

// .length property allows us to get and set the total number of items in an array


//deletes last one
console.log(mixedArray.length)
mixedArray.length = mixedArray.length -1;
console.log(mixedArray)

let theBeatles = ["John", "Paul", "ringo", "george"];
theBeatles.length++;
console.log(theBeatles);

//reading from arrays
console.log(grades[0]);

//thjis is underfined
console.log(grades[20]);

let lakersLegends = ["kobe", "shaq","lebron", "magic", "kareem"];

console.log('Array before reassignment');
console.log(lakersLegends);
lakersLegends[2] = "gasol";
console.log(lakersLegends);